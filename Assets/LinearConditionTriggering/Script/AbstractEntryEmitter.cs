﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public abstract class AbstractEntryEmitter : MonoBehaviour
{

    public OnEntryEvent m_onEntryEmitted;

    [Header("Debug")]
    public EntryEvent m_lastDetected;


    public void NotifyEntryEvent(EntryEvent e) {
        m_lastDetected = e;
        m_onEntryEmitted.Invoke(e);
    }


}
