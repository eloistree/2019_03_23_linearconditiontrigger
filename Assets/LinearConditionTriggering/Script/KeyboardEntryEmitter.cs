﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardEntryEmitter : AbstractEntryEmitter
{

    private void Update()
    {
        foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(kcode))
            {
                NotifyEntryEvent(EntryEvent.Create(kcode.ToString(), true));
            }
            if (Input.GetKeyUp(kcode))
            {
                NotifyEntryEvent(EntryEvent.Create(kcode.ToString(), false));
            }

        }
    }

   

}
