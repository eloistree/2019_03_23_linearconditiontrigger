﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityDefaultInputManagerEmitter : AbstractEntryEmitter
{
    public string[] m_buttons;
    public string[] m_axis;
    public float m_axisDeathZone=0.1f;

    private void Start()
    {
        for (int i = 0; i < m_axis.Length; i++)
        {
            m_axisState.Add("-" + m_axis[i], false);
            m_axisState.Add("+" + m_axis[i], false);

        }
    }
    

    private void Update()
    {
        foreach (string button in m_buttons)
        {
            if (Input.GetButtonDown(button))
            {
                NotifyEntryEvent(EntryEvent.Create(button, true));
            }
            if (Input.GetButtonUp(button))
            {
                NotifyEntryEvent(EntryEvent.Create(button, false));
            }

        }
        foreach (string axis in m_axis)
        {
            float a = Input.GetAxis(axis);
            bool isAxisActive = Mathf.Abs(a) > m_axisDeathZone;

            if (a<=0)
            {
                if (a < -m_axisDeathZone)
                {
                    if (m_axisState["-" + axis] == false)
                    {
                        m_axisState["-" + axis] = true;
                        NotifyEntryEvent(EntryEvent.Create("-" + axis, true));
                    }
                }
                if (a > -m_axisDeathZone)
                {
                    if (m_axisState["-" + axis] == true)
                    {
                        m_axisState["-" + axis] = false;
                        NotifyEntryEvent(EntryEvent.Create("-" + axis, false));
                    }
                }
            }
            if (a >= 0)
            {
                if (a > m_axisDeathZone)
                {
                    if (m_axisState["+" + axis] == false)
                    {
                        m_axisState["+" + axis] = true;
                        NotifyEntryEvent(EntryEvent.Create("+" + axis, true));
                    }
                }
                if (a < m_axisDeathZone)
                {
                    if (m_axisState["+" + axis] == true)
                    {
                        m_axisState["+" + axis] = false;
                        NotifyEntryEvent(EntryEvent.Create("+" + axis, false));
                    }
                }
            }
        }
    }

    public Dictionary<string, bool> m_axisState = new Dictionary<string, bool>();
    
}
