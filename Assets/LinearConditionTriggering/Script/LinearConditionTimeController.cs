﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LinearConditionStateMachine))]
public class LinearConditionTimeController : MonoBehaviour
{

    public LinearConditionStateMachine m_linearCondition;
    public float m_timeBetweenEntry = 1f;
    public float m_timeMaxForValidation = 3f;

    void Start()
    {
        m_linearCondition = GetComponent<LinearConditionStateMachine>();
    }
    
    void Update()
    {
        if (m_timeMaxForValidation > 0f && m_linearCondition.m_timeSinceLaunch > m_timeMaxForValidation)
        {
            m_linearCondition.ResetAsOrigine();
        }
        if (m_timeBetweenEntry > 0f  && m_linearCondition.m_timeSinceLastValide > m_timeBetweenEntry)
        {
            m_linearCondition.ResetAsOrigine();
        }
    }
}
