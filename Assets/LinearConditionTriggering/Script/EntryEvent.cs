﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct EntryEvent 
{
    public string m_idName;
    public bool m_activeState;

    public static EntryEvent Create(string idName, bool state)
    {
        return new EntryEvent() { m_idName = idName, m_activeState = state };
    }
}
[System.Serializable]
public class OnEntryEvent : UnityEvent<EntryEvent> {}